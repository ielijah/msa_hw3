using Microsoft.AspNetCore.Mvc;

namespace HW3.Controllers
{
    [ApiController]
    [Route("health")]
    public class HealthController : ControllerBase
    {
        [HttpGet(Name = "health")]
        public IActionResult Health()
        {
            return Ok(@"RESPONSE: { ""status"": ""OK""}");
        }
    }
}
